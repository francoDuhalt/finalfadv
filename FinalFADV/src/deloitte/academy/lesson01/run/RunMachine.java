package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;
import deloitte.academy.lesson01.entity.Products;
import deloitte.academy.lesson01.entity.ProductMachine;
import deloitte.academy.lesson01.machine.Movements;
/**
 * Class for vending machine
 * @author Franco Duhalt
 * @version 1.0
 * @since   2020-03-10
 */
public class RunMachine {
	public static final List<Products> machine = new ArrayList<Products>();
	public static final List<Products> totalSells = new ArrayList<Products>();
	public static final ProductMachine product1 = new ProductMachine("A1", "Chocolate", 10.50, 10);
	public static final ProductMachine product2 = new ProductMachine("A2", "Doritos", 15.50, 4);
	public static final ProductMachine product3 = new ProductMachine("A3", "Coca", 22.50, 2);
	public static final ProductMachine product4 = new ProductMachine("A4", "Gomitas", 8.75, 6);
	public static final ProductMachine product5 = new ProductMachine("A5", "Chips", 30.00, 10);
	public static final ProductMachine product6 = new ProductMachine("A6", "Jugo", 15.00, 2);
	public static final ProductMachine product7 = new ProductMachine("B1", "Galletas", 10.00, 3);
	public static final ProductMachine product8 = new ProductMachine("B2", "Canelitas", 120.00,6);
	public static final ProductMachine product9 = new ProductMachine("B3", "Halls", 10.10, 10);
	public static final ProductMachine product10 = new ProductMachine("B4", "Tarta", 3.14, 10);
	public static final ProductMachine product11 = new ProductMachine("B5", "Sabritas", 15.50, 0);
	public static final ProductMachine product12 = new ProductMachine("B6", "Chetos", 12.25, 4);
	public static final ProductMachine product13 = new ProductMachine("C1", "Rocaleta", 10.00, 1);
	public static final ProductMachine product14 = new ProductMachine("C2", "Rancherito", 14.75, 6);
	public static final ProductMachine product15 = new ProductMachine("C3", "Ruffles", 13.15, 10);
	public static final ProductMachine product16 = new ProductMachine("C4", "Pizza Fr�a", 22.00, 9);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		machine.add(product1);
		machine.add(product2);
		machine.add(product3);
		machine.add(product4);
		machine.add(product5);
		machine.add(product6);
		machine.add(product7);
		machine.add(product8);
		machine.add(product9);
		machine.add(product10);
		machine.add(product11);
		machine.add(product12);
		machine.add(product13);
		machine.add(product14);
		machine.add(product15);
		
		Movements movements = new Movements();
		
		
		System.out.println(movements.addProduct(machine, product16));
		System.out.println(movements.editName(machine, "B4", "Pay de Limon"));
		System.out.println(movements.editPrice(machine, "B2", 10.50));
		System.out.println(movements.editQuantity(machine, "C1", 5));
		System.out.println(movements.removeProduct(machine, "B2"));
		System.out.println(movements.sellProduct(machine, "A3", totalSells));
		System.out.println(movements.sellProduct(machine, "A3", totalSells));
		System.out.println(movements.sellProduct(machine, "A2", totalSells));
		System.out.println(movements.sellProduct(machine, "A3", totalSells));
		System.out.println(movements.sellProduct(machine, "B1", totalSells));
		System.out.println(movements.sellProduct(machine, "B1", totalSells));
		System.out.println(movements.totalSells(totalSells));
		
		
		for (Products productID: totalSells) {
			System.out.println("Producto: "+ productID.getName()+ ", Codigo: " + productID.getId() + ", Cantidad: " + productID.getQuantity() + ", Precio: " + productID.getCost());
		} 
		
		
		//System.out.println(movements.addProduct(machine, product16));
		//System.out.println(movements.removeProduct(machine, "B2"));

	}

}
