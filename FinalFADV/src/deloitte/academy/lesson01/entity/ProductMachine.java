package deloitte.academy.lesson01.entity;
/**
 * Class for product machine attributes
 * @author Franco Duhalt
 * @version 1.0
 * @since   2020-03-10
 */
public class ProductMachine extends Products{
	
	
	public ProductMachine() {
		// ProductsMachine Constructor
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductMachine(String string, String string2, double d, int i) {
		// Constructor with id, name, cost and quantity as params
		super(string, string2, d, i);
		// TODO Auto-generated constructor stub
	}
	
}
