package deloitte.academy.lesson01.entity;

import java.util.List;

/**
 * Interface for operations
 * @author Franco Duhalt
 * @version 1.0
 * @since   2020-03-10
 */
public interface Operations {
	/**
	 * Method to add an element to an arrayList
	 * @param productList
	 * @param product
	 * @exception error message for an exception occur  
	 * @return string
	 */

	public String addProduct(List<Products> productList, Products product);
	
	/**
	 * Method to remove an element from an arrayList
	 * @param productList
	 * @param id
	 * @return string
	 */
	public String removeProduct(List<Products> productList, String id);
	
	/**
	 * Method to edit the name of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param name
	 * @return string
	 */
	public String editName(List<Products> productList, String id, String name);
	
	/**
	 * Method to edit the price of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param price
	 * @return string
	 */
	public String editPrice(List<Products> productList, String id, double price);
	
	
	/**
	 * Method to edit the quantity of an element from an arrayList
	 * @param productList
	 * @param id
	 * @param quantity
	 * @return string
	 */
	public String editQuantity(List<Products> productList, String id, int quantity);
	
	/**
	 * Method to edit subtrac 1 to the quantity of an element from an arrayLists
	 * @param productList
	 * @param id
	 * @return
	 */
	public String sellProduct(List<Products> productList, String id, List<Products> sellList);
	
	
	/**
	 * Method to calculate the total quantity and the total price from an arrayList
	 * @param productList
	 * @return total quantity and total price
	 */
	public String totalSells(List<Products> productList);
	
	
	
	
	
}
