package deloitte.academy.lesson01.entity;
/**
 * Class for product attributes
 * @author Franco Duhalt
 * @version 1.0
 * @since   2020-03-10
 */
public abstract class Products {
	public String id;
	public String name;
	public double cost;
	public int quantity;
	
	public Products(String id, String name, double cost, int quantity) {
		// Constructor with id, name, cost and quantity as params
		super();
		this.id = id;
		this.name = name;
		this.cost = cost;
		this.quantity = quantity;
	}
	
	
	public Products() {
		// Products Constructor
		super();
		// TODO Auto-generated constructor stub
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
