package deloitte.academy.lesson01.entity;
/**
 * Error Messages
 * @author Franco Duhalt
 * @version 1.0
 * @since   2020-03-10
 */
public enum codes {
	ERROR01("Exception occured"), ERROR02 ("No se ha vendido nada"), ERROR03("Codigo incorrecto");
	
	String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private codes(String description) {
		this.description = description;
	}
	
	
	
}
