package deloitte.academy.lesson01.machine;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.Operations;
import deloitte.academy.lesson01.entity.ProductMachine;
import deloitte.academy.lesson01.entity.Products;
import deloitte.academy.lesson01.entity.codes;

/**
 * Class for a vending machine movements and CRUD for products
 * 
 * @author Franco Duhalt
 * @version 1.0
 * @since   2020-03-10
 * @code sellProducts(sellList, productID);
 */

public class Movements implements Operations {
	private static final Logger LOGGER = Logger.getLogger(Movements.class.getName());
	
	
	/**
	 * Method to add an element from an arrayList
	 * @Value("List<Products> productList, Products product")
	 * @param productList
	 * @param id
	 * @exception error message for an exception occur  
	 * @return string
	 */
	@Override
	public String addProduct(List<Products> productList, Products product) {
		// TODO Auto-generated method stub
		String message = "";
		Boolean flag = false;

		try {
			for (Products productID : productList) {
				if (productID.getId() == product.getId()) {
					flag = true;
					break;
				}
			}
			if (flag == false) {
				productList.add(product);
				message = "Se ha agregado el producto " + product.getName() + " con el codigo: " + product.getId();
			} else {
				message = "El producto " + product.getName() + " ya existe";
			}
		
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}

		return message;
	}
	
	/**
	 * Method to remove an element from an arrayList
	 * @Value("List<Products> productList, String id")
	 * @param productList
	 * @param id
	 * @exception error message for an exception occur  
	 * @return string
	 */
	@Override
	public String removeProduct(List<Products> productList, String id) {
		// TODO Auto-generated method stub
		String message = "";
		Products productEliminate = null;
		String productName = "";

		try {
			for (Products productID : productList) {
				if (productID.getId() == id) {
					productEliminate = productID;
					productName = productID.getName();
					productList.remove(productEliminate);
					message = "Se ha eliminado el producto " + productName;
					break;
				} else {
					message = codes.ERROR03.getDescription().toString();
				}
			}

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}

		return message;
	}

	/**
	 * Method to edit the name from an element from an arrayList
	 * @Value("List<Products> productList, String id, String name")
	 * @param productList
	 * @param id
	 * @exception error message for an exception occur
	 * @return string
	 */
	@Override
	public String editName(List<Products> productList, String id, String name) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		String productCode = "";
		String oldName = "";

		try {
			for (Products productID : productList) {
				if (productID.getId() == id) {
					oldName = productID.getName();
					productID.setName(name);
					productName = productID.getName();
					productCode = productID.getId();
					message = "Se ha modificado el nombre del producto " + "'" + oldName + "'" + " con codigo "
							+ productCode + " a " + "'" + productName + "'";
					break;
				} else {
					message = codes.ERROR03.getDescription().toString();
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}
		return message;
	}

	/**
	 * Method to edit the price from an element from an arrayList
	 * @Value("List<Products> productList, String id, double price")
	 * @param productList
	 * @param id
	 * @return string
	 * @exception error message for an exception occur
	 */
	@Override
	public String editPrice(List<Products> productList, String id, double price) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		String productCode = "";
		double oldPrice = 0;

		try {
			for (Products productID : productList) {
				if (productID.getId() == id) {
					oldPrice = productID.getCost();
					productID.setCost(price);
					productName = productID.getName();
					productCode = productID.getId();
					message = "Se ha modificado el precio '$" + oldPrice + "' del producto " + productName
							+ " con codigo " + productCode + " a '$" + price + "'";
					break;
				} else {
					message = codes.ERROR03.getDescription().toString();
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}
		return message;
	}

	/**
	 * Method to edit the quantity from an element from an arrayList
	 * @Value("List<Products> productList, String id, int quantity")
	 * @param productList
	 * @param id
	 * @exception error message for an exception occur
	 * @return string
	 */
	@Override
	public String editQuantity(List<Products> productList, String id, int quantity) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		String productCode = "";
		int oldQuantity = 0;

		try {
			for (Products productID : productList) {
				if (productID.getId() == id) {
					oldQuantity = productID.getQuantity();
					productID.setQuantity(quantity);
					productName = productID.getName();
					productCode = productID.getId();
					message = "Se ha modificado la cantidad '" + oldQuantity + "' del producto " + productName
							+ " con codigo " + productCode + " a '" + quantity + "'";
					break;
				} else {
					message = codes.ERROR03.getDescription().toString();
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}
		return message;
	}

	/**
	 * Method for subtrac 1 to the quantity of an element from an arrayLists
	 * @Value("List<Products> productList, String id, List<Products> sellList")
	 * @param productList
	 * @param id
	 * @exception error message for an exception occur
	 * @return
	 */
	@Override
	public String sellProduct(List<Products> productList, String id, List<Products> sellList) {
		// TODO Auto-generated method stub
		String message = "";
		String productName = "";
		String productCode = "";

		int total = 0;

		try {
			for (Products productID : productList) {
				if (productID.getId() == id) {
					if (productID.getQuantity() > 0) {

						total = (productID.getQuantity()) - 1;
						productID.setQuantity(total);
						productName = productID.getName();
						productCode = productID.getId();
						/**
						 * @see sellProducts()
						 */
						sellProducts(sellList, productID);
						message = "Se ha vendido " + productName + " con codigo " + productCode;

						break;
					} else if (productID.getQuantity() == 0) {
						
						productName = productID.getName();
						productCode = productID.getId();
						message = "El producto " + productName + " con codigo " + productCode + " no esta disponible";

						break;
					}
				} else {
					message = "El codigo " + productCode + " esta incorrecto";
				}

			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}
		return message;
	}

	/**
	 * Method to calculate the total quantity and the total price from an arrayList
	 * @Value("List<Products> productList")
	 * @param productList
	 * @thows error message for an exception occur
	 * @return summ of quantity and sum of price
	 */
	@Override
	public String totalSells(List<Products> productList) {
		// TODO Auto-generated method stub
		String message = "";
		int total = 0;
		double sells = 0;
		try {
			if (productList.size() > 0) {
				for (Products product : productList) {
					total += product.getQuantity();
					sells += product.getCost();
					message = "El total de ventas del d�a es: " + total + " productos y $" + sells;
				}
			} else {
				message = codes.ERROR02.getDescription().toString();
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}

		return message;
	}
	/**
	 * Method for add an element to an arrayList of type Products
	 * @Value("List<Products> sellList, Products product")
	 * @param sellList
	 * @param product
	 * @throws error message for an exception occur
	 * @return arrayList with a new element
	 */
	public static void sellProducts(List<Products> sellList, Products product) {
		try {
			ProductMachine productsHistorial = new ProductMachine(product.getId(), product.getName(), product.getCost(), 1) ;
			sellList.add(productsHistorial);
		
		}catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}
	}
	/**
	 * Method for add an element to an arrayList of type Products
	 * @Value("List<Products> sellList, Products product")
	 * This method is no longer acceptable.
	 * @deprecated
	 * @param sellList
	 * @param product
	 * @throws error message for an exception occur
	 * @return arrayList with a new element
	 */
	@Deprecated
	public static void productSell(List<Products> sellList, Products product) {
		try {
			ProductMachine productsHistorial = new ProductMachine(product.getId(), product.getName(), product.getCost(), 1) ;
			sellList.add(productsHistorial);
		
		}catch (Exception e) {
			LOGGER.log(Level.SEVERE, codes.ERROR01.getDescription().toString(), e);
		}
	}
}
